<?php

namespace Drupal\gtfs_schedule\Routing;

use Drupal\gtfs_schedule\Form\GTFSScheduleConfigForm;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class GTFSScheduleRouter {

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $routes = [];
    $path = \Drupal::config(GTFSScheduleConfigForm::SETTINGS)->get('base_url');
    // Declares a single route under the name 'example.content'.
    // Returns an array of Route objects.
    $routes['gtfs_schedule.schedule'] = new Route(
      '/' . $path . '/{route_id}',
      [
        '_controller' => '\Drupal\gtfs_schedule\Controller\GTFSScheduleController::content',
        '_title_callback' => '\Drupal\gtfs_schedule\Controller\GTFSScheduleController::title'
      ],
      [
        '_permission'  => 'view gtfs_schedule',
      ]
    );
    return $routes;
  }

}
