<?php

namespace Drupal\gtfs_schedule\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class GTFSScheduleConfigForm extends ConfigFormBase {
  const SETTINGS = 'gtfs_schedule.settings';

  /**
   *
   */
  public function getFormId() {
    return 'gtfs_schedule_settings';
  }

  /**
   *
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['gtfs_server_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GTFS server URL'),
      '#description' => $this->t('A Drupal GTFS server URL that has parsed a GTFS feed.'),
      '#default_value' => $config->get('gtfs_server_url'),
    ];

    if ($config->get('gtfs_server_url')) {
      $url = $config->get('gtfs_server_url') . '/gtfs/api/v1';
      if ($config->get('feed_id')) {
        $url .= '?feed_id=' . $config->get('feed_id');
      }
      $form['gtfs_server_url']['#description'] .=
        '<br />' . $this->t('Current URL Endpoint: :url', [':url' => $url]);
    }

    $form['agencies'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Agency IDs'),
      '#description' => $this->t('A comma-separated list of agency IDs for which to provide schedules'),
      '#default_value' => $config->get('agencies')
    ];

    $form['route_name_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Route Name Type'),
      '#default_value' => $config->get('route_name_type'),
      '#options' => [
        'route_id' => $this->t('Route ID'),
        'route_short_name' => $this->t('Route Short Name'),
        'route_long_name' => $this->t('Route Long Name')
      ],
      '#description' => $this->t('Determines what route name type will be used in the URL'),
    ];

    $form['base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URL for schedule pages'),
      '#default_value' => $config->get('base_url'),
      '#description' => $this->t('The base URL from which you would like to see the schedules listed'),
    ];

    $form['timeformat'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time format'),
      '#default_value' => $config->get('timeformat'),
      '#description' => $this->t('Schedule time format for stoptimes. Current format example: %example', ['%example' => date($config->get('timeformat'))]),
    ];

    $form['empty_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Empty Value Placeholder'),
      '#default_value' => $config->get('empty_placeholder'),
      '#description' => $this->t('The text shown when there is no stoptime for a given trip x stop combination.'),
    ];

    $form['timepoints_only_default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Default to show only stops that are flagged as timepoints'),
      '#default_value' => $config->get('timepoints_only_default'),
      '#description' => $this->t('Check this box to limit a schedule to stops that have defined timepoints. This can be overridden using timepointsOnly=false parameter.')
    ];

    $form['no_schedule_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('No Schedule Message'),
      '#default_value' => $config->get('no_schedule_message'),
      '#description' => $this->t('Error message returned when schedule data is not available.'),
    ];

    $form['request_cache_age_limit'] = [
      '#type' => 'select',
      '#title' => $this->t('Request cache age limit'),
      '#default_value' => $config->get('request_cache_age_limit', 0),
      '#options' => [
        '0' => $this->t('Do not use cache'),
        '60' => $this->t('1 minute'),
        '300' => $this->t('5 minutes'),
        '600' => $this->t('10 minutes'),
        '1800' => $this->t('30 minutes'),
        '3600' => $this->t('60 minutes'),
        '21600' => $this->t('6 hours'),
        '43200' => $this->t('12 hours'),
        '86400' => $this->t('24 hours'),
      ],
      '#description' => $this->t('How long to use cached request values before making a new request.'),
    ];

    $form['feed_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Feed ID'),
      '#default_value' => $config->get('feed_id'),
      '#description' => $this->t('Leave empty for default feed, or add feed ID, or add `latest`.'),
    ];

    $form['regenerate_cache_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Regenerate Cache Key'),
      '#default_value' => $config->get('regenerate_cache_key'),
      '#description' => $this->t('Secret key to allow regenerating of cache from remote endpoint. Leave blank to allow all requests to invalidate cache.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('gtfs_server_url', $form_state->getValue('gtfs_server_url'))
      ->set('agencies', $form_state->getValue('agencies'))
      ->set('route_name_type', $form_state->getValue('route_name_type'))
      ->set('base_url', $form_state->getValue('base_url'))
      ->set('timeformat', $form_state->getValue('timeformat'))
      ->set('empty_placeholder', $form_state->getValue('empty_placeholder'))
      ->set('timepoints_only_default', $form_state->getValue('timepoints_only_default'))
      ->set('no_schedule_message', $form_state->getValue('no_schedule_message'))
      ->set('request_cache_age_limit', $form_state->getValue('request_cache_age_limit'))
      ->set('feed_id', $form_state->getValue('feed_id'))
      ->set('regenerate_cache_key', $form_state->getValue('regenerate_cache_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
