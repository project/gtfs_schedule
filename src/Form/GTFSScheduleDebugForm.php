<?php

namespace Drupal\gtfs_schedule\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class GTFSScheduleDebugForm extends FormBase {
  public function getFormId() {
    return 'gtfs_schedule_debug_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $directory = GTFS_SCHEDULE_CACHE_LOCATION;
    $form['actions']['#type'] = 'actions';
    if (file_exists($directory)) {
      $files = scandir($directory);
      if (count($files) > 2) {
        $form['clear_cache'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Delete GTFS schedule request cache (count: :count)', [':count' => (count($files) - 2)]),
          '#name' => 'clear_cache',
        ];
        $form['invalidate_cache'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Invalidate GTFS schedule request cache (count: :count)', [':count' => (count($files) - 2)]),
          '#name' => 'invalidate_cache',
        ];
      }
    }

    $form['clear_url_cache'] = [
      '#type' => 'textfield',
      '#name' => 'clear_url_cache',
      '#title' => 'Clear cache for URL',
      '#maxlength' => 512,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->getValue('clear_cache')) {

      // Clear cache.
      gtfs_schedule_modify_cache('clear_cache');
    } elseif ($form_state->getValue('invalidate_cache')) {
      // Invalidate cache.
      gtfs_schedule_modify_cache('invalidate_cache');
    }

    $url = $form_state->getValue('clear_url_cache');

    if (!empty($url)) {
      $directory = GTFS_SCHEDULE_CACHE_LOCATION;
      $full_path = $directory . '/' . md5($url) . '.serialized';
      if(!file_exists($full_path)) {
        \Drupal::messenger()->addWarning($this->t('URL %url not cached', ['%url' => $url]));
      } else {
        unlink($full_path);
        \Drupal::messenger()->addMessage($this->t('Deleted %url', ['%url' => $url]));
      }
    }
  }

}