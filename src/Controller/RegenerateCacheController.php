<?php

namespace Drupal\gtfs_schedule\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\gtfs_schedule\Form\GTFSScheduleConfigForm;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

class RegenerateCacheController extends ControllerBase {

  /**
   * @var RequestStack
   */
  protected $requestStack;

  /**
   * @var ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new instance of the callback controller.
   *
   * @param FileSystemInterface $file_system
   * @param RequestStack $request_stack
   */
  public function __construct(RequestStack $request_stack, ModuleHandlerInterface $module_handler) {
    $this->requestStack = $request_stack;
    $this->moduleHandler = $module_handler;
  }

  public function process() {
    $directory = GTFS_SCHEDULE_CACHE_LOCATION;
    if(file_exists($directory)) {
      foreach(scandir($directory) as $file) {

        // Skip directories.
        if(is_dir($file)) {
          continue;
        }

        $path = "{$directory}/{$file}";

        gtfs_schedule_modify_path($path, 'invalidate_cache');

      }
    }
    return new JsonResponse(['success' => true]);
  }

  public function access() {
    $method = $this->requestStack->getCurrentRequest()->getMethod();
    if ('POST' !== $method) {
      return AccessResult::forbidden('Method not allowed');
    }
    $authorization = $this->requestStack->getCurrentRequest()->headers->get('Authorization');
    $authorization = trim(str_replace('Bearer ', '', $authorization));
    $lock = $this->config(GTFSScheduleConfigForm::SETTINGS)->get('regenerate_cache_key');
    if ($lock && $authorization !== $lock) {
      return AccessResult::forbidden('Invalid secret');
    }
    $content_type = $this->requestStack->getCurrentRequest()->headers->get('Content-Type');
    if ('application/json' !== $content_type) {
      return AccessResult::forbidden('Invalid content type');
    }
    return AccessResult::allowed();
  }
}