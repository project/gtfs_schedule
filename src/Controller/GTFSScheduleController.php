<?php

namespace Drupal\gtfs_schedule\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\gtfs_schedule\Form\GTFSScheduleConfigForm;

/**
 * An example controller.
 */
class GTFSScheduleController extends ControllerBase {

  public static function titleFromRouteId($route_id) {

    $route = gtfs_schedule_get_route($route_id);
    if ($route) {
      $title = "{$route->route_short_name} {$route->route_long_name}: Schedules";
    } else {
      $title = "$route_id: Schedules";
    }

    return $title;
  }

  public function title(string $route_id) {
    $title = static::titleFromRouteId($route_id);
    \Drupal::moduleHandler()->alter('gtfs_schedule_title', $title, $route_id);
    return $title;
  }

  public function content(string $route_id) {
    return static::getContent($route_id);
  }

  /**
   * Returns a render-able array for a test page.
   */
  public static function getContent(string $route_id) {
    $config = \Drupal::config(GTFSScheduleConfigForm::SETTINGS);

    $date_format = $config->get('timeformat');

    $route = gtfs_schedule_get_route($route_id);

    if (!$route_id || !$route) {
      return [
        '#markup' => $config->get('no_schedule_message')
      ];
    }

    $arguments['timeFormat'] = $config->get('timeformat');

    if ($config->get('timepoints_only_default')) {
      $arguments['filter'] = ['timepoint' => ['=' => '1']];
    }

    \Drupal::moduleHandler()->alter('gtfs_schedule_request_arguments', $arguments);

    $version = 'source';

    \Drupal::moduleHandler()->alter('gtfs_schedule_version', $version, $route_id);

    try {
      $schedule = gtfs_schedule_request("agencies/{$route->agency_id}/routes/{$route->route_id}/schedules", $arguments, true, null, $version);
    } catch (\Throwable $e) {

    }


    if (empty($schedule->data)) {
      return [
        '#markup' => $config->get('no_schedule_message')
      ];
    }

    if (!isset($_GET['direction_id']) || !isset($_GET['service_id'])) {
      return [
        'gtfs_schedule_header' => [
          '#theme' => 'gtfs_schedule_table',
          '#schedules' => $schedule,
          '#route_url_id' => $route->route_id
        ]
      ];
    }

    // We have a direction and service id; we can now build the table

    $caption = 'test';
    $header = [];

    foreach ($schedule->data->stops as $stop) {
      $header[] = [
        'data' => [
          '#markup' => "<a href='/1{$stop->stop_id}' class='notranslate'>{$stop->stop_name}</a>"
        ],
        'scope' => 'col'
      ];
    }

    $rows = [];
    $last = 0;
    $next_day = 0;

    foreach ($schedule->data->trips as $row) {
      if (!count($row->stop_times)) {
        continue;
      }

      $data = array_map(function ($stop) {
        return isset($stop->arrival_time) ? $stop->arrival_time : '';
      }, $row->stop_times);

      $filtered = array_filter($data);
      $max_date = date_create_from_format($date_format, end($filtered));
      $timestamp = $max_date ? $max_date->getTimestamp() : 999999999999;

      if ($timestamp < $last) {
        $next_day = 60 * 60 * 24;
      }

      $timestamp += $next_day;

      $last = $timestamp;

      foreach ($data as $key => $value) {
        if ($value == '') {
          $data[$key] = $config->get('empty_placeholder');
        }
      }

      $rows[] = [
        'id' => $row->trip_id,
        'data-max-time' => $timestamp,
        'data' => $data
      ];
    }


    \Drupal::moduleHandler()->alter('gtfs_schedule_data', $schedule);


    $build = [
      'gtfs_schedule_header' => [
        '#theme' => 'gtfs_schedule_header',
        '#schedule' => $schedule,
        '#route_url_id' => $route->route_id
      ],
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => ['class' => ['h1']],
        '#value' => static::titleFromRouteId($route_id)
      ],
      'gtfs_schedule_table' => [
        '#theme' => 'table',
        '#sticky' => true,
        '#attributes' => [
          'id' => 'route_schedule',
          'data-tablesaw-mode' => 'swipe',
          'data-tablesaw-minimap' => '',
          'class' => 'tablesaw tablesaw-swipe'
        ],
        '#header' => $header,
        '#rows' => $rows,
      ]
    ];

    \Drupal::moduleHandler()->alter('gtfs_schedule_output', $build, $route_id);

    if (empty($build['gtfs_schedule_table']['#rows'])) {
      if (is_string($schedule->data->service)) {
        $build['gtfs_schedule_table']['#suffix'] = $schedule->data->service;
      } else {
        $build['gtfs_schedule_table']['#suffix'] = $config->get('no_schedule_message');
      }
    }

    return $build;
  }

}
